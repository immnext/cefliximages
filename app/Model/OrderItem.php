<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $guarded = [];

    public function order(){
        return $this->belongsTo("App\Model\Order",'order_id','unique_id');
    }

    public function product(){
        return $this->hasMany("App\Model\Product",'product_id','unique_id');
    }
}
