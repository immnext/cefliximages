<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    public function getRouteKeyName()
    {
        return 'unique_id';
    }

    protected $guarded = [];


    public function product(){
        return $this->belongsTo("App\Model\Product",'product_id','unique_id');
    }
}
