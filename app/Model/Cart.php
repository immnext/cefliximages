<?php

namespace App\Model;

use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;

class Cart
{
    public  $cart = array();
    public  $totalPrice = 0;

    public function  __construct($oldCart)
    {
        $this->cart = ($oldCart) ? $oldCart : [];
    }

    public function add($item){

        $storedItem = Session::get('cart');
        if(!in_array($item, $storedItem)){
            array_push($storedItem, $item);

            $this->cart = $storedItem;
            Session::put('cart', $storedItem);

            $resp = [
                'status' => Response::HTTP_OK,
                'message' => 'Added to cart'
            ];
        } else {
            $resp = [
                'status' => Response::HTTP_CONFLICT,
                'message' => 'Product exists in cart already'
            ];
        }

        return $resp;
    }
}
