<?php

namespace App\Http\Middleware;

use App\Model\Store;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class CheckStoreOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
       $store = Store::where("unique_id",$request -> store_id)->first();

        if($store->owner_id != auth()->user()->unique_id){
            return response('Sorry, you have no permission to view this store', Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}
