<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    public function profile()
    {
        return auth()->user();
    }



    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $this->validate($request,[
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users,email,'.$user->id],
            'password' => ['sometimes','required', 'string', 'min:8'],
        ]);

        $currentPhoto = $user->photo;

        if ($request->photo != $currentPhoto){
//            $name = time(). '.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            $name = time(). '.' . request()->photo->getClientOriginalExtension();

            Image::make($request->photo)->save(public_path('images/profile/').$name);

            $request->merge(['photo' => $name]);

            $userPhoto = public_path('images/profile/').$currentPhoto;

            if (file_exists($userPhoto)){
                @unlink($userPhoto);
            }
        }

        if (!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }


        $user->update($request->all());

        return ['message' => 'Profile Updated'];
    }
}
