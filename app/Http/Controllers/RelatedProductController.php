<?php

namespace App\Http\Controllers;

use App\Http\Resources\RelatedProductResource;
use App\Model\Product;
use App\Model\RelatedProduct;
use Illuminate\Http\Request;

class RelatedProductController extends Controller
{
    public function related_products(){
        $products = Product::get();

        foreach ($products as $product){
            $relatedProduct = Product::where('category_id', $product->category_id)
                ->Orwhere('store_id', $product->store_id)->get();
        }

        if(isset($_GET['num'])){
            $num = $_GET['num'];

            $resource =  RelatedProductResource::collection($relatedProduct->take($num));

        }else{

            $resource =  RelatedProductResource::collection($relatedProduct);
        }

        if($resource->count() > 0)
            return response()->json([
                'status'=> true,
                'message'=>'data returned',
                'data'=> $resource
            ]);

        else

            return response()->json([
                'status'=> false,
                'message'=>'No data returned',
            ]);

    }
}
