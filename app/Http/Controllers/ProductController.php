<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Model\Cart;
use App\Model\Product;
use App\Model\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;
use Webpatser\Uuid\Uuid;
use S3;

class ProductController extends Controller
{
    public function index(Product $product){

        if(isset($_GET['num'])){
            $num =$_GET['num'];

            //$resource =  Product::take($num)->get();
            $resource =  ProductResource::collection($product->orderby('id','desc')->take($num)->get());

        }else{
            $resource =  ProductResource::collection($product->latest()->get());

        }

        if($resource->count() > 0)
            return response()->json([
                'status'=> true,
                'message'=>'data returned',
                'data'=> $resource
            ]);
        else
            return array('status'=> false,'message'=>'No data returned');

        // return ProductResource::collection($product->latest()->get());
    }

    public function store_product(Request $request, Product $product){

        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'required|image|mimes:jpeg,jpg|max:2048',
            'product_file' => 'required',
        ]);

        $user_store = Store::where("owner_id",Auth::user()->unique_id)->first()->unique_id;

        $imageName = time(). '.' . request()->image->getClientOriginalExtension();

        $product_fileName = time(). '.' . request()->product_file->getClientOriginalExtension();


        $s3 = new S3('AKIAYIMTQ7ZNUX4GSC57','kNc/d572ntscpDWcwamoTdA8nfqKiZymzBZ6RbgT' );

        if ($s3->putObjectFile($request->file('image')->path(), "vcp-blw", "timeline/cei/products/images/" . $imageName,
            S3::ACL_PUBLIC_READ)) {
            $product_image = "http://vcp-blw.s3.amazonaws.com/timeline/cei/products/images/".$imageName;
        }

        if ($s3->putObjectFile($request->file('product_file')->path(), "vcp-blw", "timeline/cei/products/files/" .
            $product_fileName,
            S3::ACL_PUBLIC_READ)) {
            $product_file = "http://vcp-blw.s3.amazonaws.com/timeline/cei/products/files/".$product_fileName;
        }



        $data = array(
            'category_id' => "84c3de71-f093-3145-8c34-1f7925856d2d",
            'store_id' => $user_store,
            'unique_id' => Uuid::generate()->string,
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'description' => $request->description,
            'price' => $request->price,
            'type' => $request->type,
            'status' => 1,
            'image' => $product_image,
            'file' => $product_file,
        );


        $product->create($data);


        return response('Product has been created', Response::HTTP_CREATED);
    }


    public function show_product(Product $product_id){
        if (empty($product_id))
            return response('Product not found', Response::HTTP_NOT_FOUND);
        else
            return new ProductResource($product_id);
    }


    public function update(Request $request, Product $product_id){
        if (empty($product_id))
            return response('Product not found', Response::HTTP_NOT_FOUND);
        else
            $product_id->update($request->all());
        return response('Product has been updated', Response::HTTP_OK);
    }

    public function delete(Product $product_id){
        $product_id->delete();
        return response('Product has been deleted', Response::HTTP_NO_CONTENT);
    }

    public function getAddToCart(Request $request, $id){
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $add = $cart->add($product);



//        $request->session()->put('cart', $cart);

//        dd($request->session()->get('cart'));

        return response($add['message'], $add['status']);
    }

    public function  getCart(){
        if (!Session::get('cart')){
            return response('No item in cart',Response::HTTP_NOT_FOUND);
        }

        $cart = Session::get('cart');

//        print_r($cart);

        $totalPrice = 0;
        for($i = 0; $i < count($cart); $i++){
            $totalPrice += $cart[$i]->price;
        }

        return response(['products' => $cart, 'total_price' => $totalPrice]);
    }





}
