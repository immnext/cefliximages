<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\StoreResource;
use App\Model\OrderItem;
use App\Model\Product;
use App\Model\Store;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;
use Webpatser\Uuid\Uuid;

class StoreController extends Controller
{
    public function index(Store $store){
        return StoreResource::collection($store->latest()->get());
    }

    public function create_store(Request $request){
        $request->validate([
            'name' => 'required',
        ]);

//        $imageName = time(). '.' . explode('/', explode(':', substr($request->profile_pic, 0, strpos($request->profile_pic, ';')))[1])[1];
//
//        Image::make($request->profile_pic)->save(public_path('images/').$imageName)->resize(1024, 370);

        $newStore = new Store();

        $newStore->owner_id = Auth::user()->unique_id;
        $newStore->unique_id = Uuid::generate()->string;
        $newStore->name = $request->name;
        $newStore->slug = str_slug($request->name);
        $newStore->profile_pic = $request->profile_pic;//$imageName
        $newStore->save();

        User::where('unique_id', auth()->user()->unique_id)->update([
            'store_id' => $newStore->unique_id
        ]);

        return response()->json([
            'status'=>true,
            'message' => 'Store has been created',
            'data' => $newStore->unique_id

        ],200);

    }

    public function store_products(){

        if(isset($_GET['num'])) {
            $num = $_GET['num'];
            $fetchedProducts = Product::where('store_id', auth()->user()->store_id)->take($num)->get();
            $storeProducts = ProductResource::collection($fetchedProducts);
        }else{

            $fetchedProducts = Product::where('store_id', auth()->user()->store_id)->get();
            $storeProducts = ProductResource::collection($fetchedProducts);

        }

            return response()->json([
                'status' => true,
                'data' => $storeProducts,
            ]);
    }

    public function show(Request $request){

        //validation
        $store = Store::where("owner_id",Auth::user()->unique_id)->first();

        if(!empty($store)){

            return response()->json([
                'status' => true,
                'message' => "Store successfully loaded",
                'data' => $store,
            ]);

        }else{

            return response()->json([
                'status' => false,
                'message' => "You do not have a store",
                'data' => $store,
            ]);

        }


    }

    public function update(Request $request){

        $store = Store::where("owner_id",Auth::user()->unique_id)->first();
        $store -> name = $request ->name;
        $store -> save();

        return response()->json([
            'status' => true,
            'messgae' => "Store successfully updated",
            'data' => $store,
        ]);

    }

    public function delete(Store $store_id){
        $store_id->delete();
        return response('Store has been deleted', Response::HTTP_OK);
    }


    public function storeSuccessOrder()
    {
        $store = Store::where("owner_id",Auth::user()->unique_id)->first();

        if(empty($store)){

            return response()->json([
                'status' => false,
                'message' => "You don't have a create yet, create one to proceed"
            ]);

        }else{

            $items = OrderItem::where("store_id", $store->unique_id)
                ->where('status',1)
                -> get();

            if(count($items) > 0){

                return response()->json([
                    'status' => true,
                    'message' => "Items loaded",
                    'data' => $items
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => "This order has no items"
                ]);
            }
        }

    }
}
