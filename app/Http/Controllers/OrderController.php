<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderItemResource;
use App\Http\Resources\OrderResource;
use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderItem;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Response;
use Webpatser\Uuid\Uuid;
use Validator;

class OrderController extends Controller
{
    public function orders(Order $order){
        return OrderResource::collection($order->latest()->get());
    }

    public function save_order(Request $request){

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['required', 'string', 'max:191'],
            'country' => 'required',
            'street_address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip_code' => 'required',
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => 'required',
            'products' =>'required'

        ]);


        if ($validator->fails()) {

            $errors =$validator->errors()->all();

            return response()->json([
                'status'=> false,
                'message' => 'Some error(s) occurred',
                'errors'=> $errors

            ]);

        }





        //convert products to array
        $orders = explode(",", $request->products);

        //pass the products ids into the fetch clause
        $products = Product::whereIn('unique_id',$orders)->get();






        if(count($products) > 0){

            $total_price = 0;
            foreach($products as $product){
                $total_price += $product->price;
            }

            $order_unique_id =  Uuid::generate()->string;

            $newOrder = new Order();

            $newOrder->user_id = Auth::user()->unique_id;
            $newOrder->unique_id = $order_unique_id;
            $newOrder->total_amount = $total_price;
            $newOrder->status = 2;
            $newOrder->country = $request->country;
            $newOrder->first_name = $request->first_name;
            $newOrder->last_name = $request->last_name;
            $newOrder->street_address = $request->street_address;
            $newOrder->city = $request->city;
            $newOrder->state = $request->state;
            $newOrder->zip_code = $request->zip_code;
            $newOrder->email = $request->email;
            $newOrder->phone = $request->phone;
            $newOrder->save();




            $orderItems = [];
            foreach ($products as $product) {
                $orderItems[] = [
                    'order_id' => $order_unique_id,
                    'store_id' => $product->store_id,
                    'product_id' => $product->unique_id,
                    'product_amount' => $product->price,
                    'product_image' => $product->image,
                    'product_title' => $product->title,
                ];
            }


            OrderItem::insert($orderItems);




            return response()->json([
                'order_id' => $order_unique_id,
                'order_amount' => $total_price,
                'status' => true,
            ]);

        } else {

            return response()->json([
                'status' => false,
                'message' => 'No items in cart',
            ]);
        }



    }

    public function show_order(Request $request){

        $order = Order::where('unique_id',$request->order_id)->first();

        if (empty($order))

            return response()->json([
                'message'=>'Order not found',
                'status' => false
            ]);

        else

            return response()->json([
                'status' => true,
                'data' => $order,
                'order_items'=>$order->orderItem,
            ]);


    }


    public function show_user_orders (){

        $num_of_item = 0;
        $order_items = [];

        // '2121d1fd-e66e-30ea-b73e-10011d00438b' instead of "auth()->user()->unique_id"
        $userOrders = Order::where('user_id',auth()->user()->unique_id)->get();

        foreach ($userOrders as $order){
            foreach ($order->orderItem as $item){

                $order_items[$num_of_item] = $item;

                $num_of_item++;
            }
        }

        if (empty($userOrders))

            return response()->json([
                'message'=>'Order not found',
                'status' => false
            ]);

        else

            return response()->json([
                'status' => true,
                'data' => $userOrders,
                'num_items' => count($order_items)
            ]);

    }


    public function show_user_order_items(){
        $i = 0;

        $items_array = [];

        //jjjj
        $orders = Order::where('user_id',auth()->user()->unique_id)->latest()->get();

        foreach ($orders as $order){
            foreach ($order->orderItem as $item){

                $items_array[$i] = $item;

                $i++;
            }
        }

        return response()->json([
            'status' => true,
            'data' => $items_array,
            'num_items' => count($items_array),
        ]);
    }


    public function order_items(Request $request){

        $order = Order::where('unique_id',$request->order_id)->first();

        return response()->json([
            'status' => true,
            'data' => $order,
            'order_items'=>$order->order_item,
        ]);

    }

    public function save_order_item(Request $request, OrderItem $orderItem){
        return $orderItem->create($request->all());
    }


    public function stream_file($product_id)
    {
        $product = Product::where("unique_id",$product_id)->first();

        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods: GET, POST');
        header("Access-Control-Allow-Headers: X-Requested-With");

        readfile($product->file);
    }


}
