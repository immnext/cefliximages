<?php

namespace App\Http\Controllers;

use App\Model\Cart;
use App\Model\Order;
use App\Model\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Unicodeveloper\Paystack\Facades\Paystack;

class PaymentController extends Controller
{


    public function confirm_payment()
    {


        $curl = curl_init();
        $reference = isset($_GET['reference']) ? $_GET['reference'] : '';
        if (!$reference) {
            die('No reference supplied');
        }


        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer " . env("PAYSTACK_SECRET"),
                "cache-control: no-cache"

            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if ($err) {
            // there was an error contacting the Paystack API
            die('Curl returned error: ' . $err);
        }

        $tranx = json_decode($response);

        //dd($tranx);

        if (!$tranx->status) {
            // there was an error from the API
            die('API returned error: ' . $tranx->message);
        }

        if ('success' == $tranx->data->status) {


            $unique_id = $tranx->data->reference;

            $order = Order::where("unique_id", $unique_id)->first();


            //check if order has already been paid for
            if ($order->status == 1) {

                return response()->json([
                    'status' => false,
                    'message' => "Value already given for this order, please check your account or contact support for further enquires"
                ]);

                exit;

                //send back error message
            }


            //check if the correct owner is requesting for the order
            if ($order->user_id != Auth::user()->unique_id) {

                return response()->json([
                    'status' => false,
                    'message' => "Sorry you do not have the right privilege to access this information"
                ]);

                exit;
                //send back error message
            }


            //give value to the user
            $order->status = 1;
            $order->save();


            $order_items = OrderItem::where("order_id", $order->unique_id)->get();

            $items_array = array();

            //Convert from object to simple array
            foreach ($order_items as $order_item) {
                $items_array[] = $order_item->id;

            }

            $activi = OrderItem::whereIn('id', $items_array)->update(['status' => 1]);


            return response()->json([
                'status' => true,
                'message' => "Payment successfully"
            ]);


        } else {

            $order = Order::where("unique_id", $tranx->data->reference)->first();
            $order->status = 0;
            $order->save();


            return response()->json([
                'status' => false,
                'message' => "Sorry there an error occurred while processing your transaction, please try again later"
            ]);


        }
    }
}



