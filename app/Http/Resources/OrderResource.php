<?php

namespace App\Http\Resources;

use App\Model\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'unique_id' => $this->unique_id,
            'total_amount' => $this->total_amount,
            'status' => $this->status,
//            'order_items' => OrderItem::where('order_id', $this->unique_id)->get(),
            'country' => $this->country,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'street_address' => $this->street_address,
            'city' => $this->city,
            'state' => $this->state,
            'zip_code' => $this->zip_code,
            'email' => $this->email,
            'phone' => $this->phone,
            'ordered date and time' => $this->created_at->format('M d, Y,H:i:s'),
        ];
    }
}
