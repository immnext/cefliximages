<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/testing123', function (Request $request) {
//    return "I got here and it is a test tht will be deleted in future";
//})->middleware('auth');

Route::get('/search', 'SearchController@search')->name('search');


Route::get('/profile', 'UserController@profile')->name('profile');
Route::patch('/profile', 'UserController@updateProfile')->name('updateProfile');


Route::get('/categories', 'CategoryController@index')->name('index_category');
Route::post('/category/products', 'CategoryController@category_products')->name('category_products');
Route::post('/category', 'CategoryController@store')->name('store_category');
Route::get('/category/{category_id}', 'CategoryController@show_category')->name('show_category');
Route::patch('/category/{category_id}', 'CategoryController@update')->name('update_category');
Route::delete('/category/{category_id}', 'CategoryController@delete')->name('delete_category');


Route::get('/products', 'ProductController@index')->name('index_product');
Route::post('/product/store', 'ProductController@upload_product')->name('store_product');
Route::post('/product/show', 'ProductController@show_product')->name('show_product');
Route::patch('/product/{product_id}', 'ProductController@update')->name('update_product');
Route::delete('/product/{product_id}', 'ProductController@delete')->name('delete_product');


Route::get('/best-categories', 'BestCategoryController@index')->name('index_best_category');
Route::post('/best-category', 'BestCategoryController@store_BestCategory')->name('store_best_category');
Route::delete('/best-category/{best_category_id}', 'BestCategoryController@delete')->name('delete_best_category');


Route::get('/featured-products', 'FeaturedProductController@index')->name('index_featured_product');
Route::post('/featured-product', 'FeaturedProductController@store_FeaturedProduct')->name('store_featured_product');
Route::delete('/featured-product/{featured_product_id}', 'FeaturedProductController@delete')->name('delete_featured_product');


Route::get('/promo-products', 'PromoProductController@index')->name('index_promo_product');
Route::post('/promo-product', 'PromoProductController@store_PromoProduct')->name('store_promo_product');
Route::delete('/promo-product/{promo_product_id}', 'PromoProductController@delete')->name('delete_promo_product');

//Route::get('/check-promo', 'PromoProductController@check_product_promo_date')->name('check_promo');

Route::get('/related-products', 'RelatedProductController@related_products')->name('index_related_product');



Route::get('/most-viewed', 'MostViewedProductController@index')->name('index_most_view');
Route::post('/most-viewed', 'MostViewedProductController@store_MostView')->name('store_most_view');
Route::delete('/most-viewed/{most_viewed_id}', 'MostViewedProductController@delete')->name('delete_most_view');


Route::get('/product-reviews', 'ProductReviewController@product_reviews')->name('index_product_review');
Route::post('/product-review', 'ProductReviewController@store_product_review')->name('store_product_review');
Route::get('/product-review/{productReview_id}', 'ProductReviewController@show_product_review')->name('show_product_review');
Route::delete('/product-review/{productReview_id}', 'ProductReviewController@delete')->name('delete_product_review');


Route::get('/sponsored', 'SponsoredProductController@index')->name('index_sponsored');
Route::post('/sponsored', 'SponsoredProductController@store_SponsoredProduct')->name('store_sponsored');
Route::delete('/sponsored/{sponsored_id}', 'SponsoredProductController@delete')->name('delete_sponsored');


Route::get('/side-banners', 'BannerController@side_banners')->name('index_side_banner');
Route::post('/side-banner', 'BannerController@store_side_banner')->name('store_side_banner');
Route::get('/side-banner/{sideBanner_id}', 'BannerController@show_side_banner')->name('show_side_banner');
Route::delete('/side-banner/{sideBanner_id}', 'BannerController@delete_banner')->name('delete_side_banner');


Route::get('/people-choices', 'BannerController@people_choice')->name('index_peopleChoice');
Route::post('/people-choice', 'BannerController@store_PeoplesChoice')->name('store_peopleChoice');
Route::delete('/people-choice/{people_choice_id}', 'BannerController@delete_peoples_choice')->name('delete_peopleChoice');


Route::get('/home-sliders', 'BannerController@home_sliders')->name('index_homeslider');
Route::post('/home-slider', 'BannerController@store_home_slider')->name('store_homeslider');
Route::delete('/home-slider/{home_slider_id}', 'BannerController@delete_home_slider')->name('delete_homeslider');


Route::get('/add-to-cart/{product_id}', 'ProductController@getAddToCart')->name('add-to-cart');
Route::get('/shopping-cart', 'ProductController@getCart')->name('getCart');


Route::get('/checkout-page', 'PaymentController@getCheckout')->name('getCheckout');
//Route::post('/checkout', 'PaymentController@checkout')->name('checkout');


Route::get('/product-images', 'ProductImageController@index')->name('index_productImg');
Route::post('/product-image', 'ProductImageController@store_productimage')->name('store_productImg');
Route::get('/product-image/{image_id}', 'ProductImageController@show_productimage')->name('show_productImg');
Route::patch('/product-image/{image_id}', 'ProductImageController@update')->name('update_productImg');
Route::delete('/product-image/{image_id}', 'ProductImageController@delete')->name('delete_productImg');







Route::get('/transactions', 'TransactionController@index')->name('transac');
Route::get('/transaction/{transaction_id}', 'TransactionController@show')->name('show_transac');
Route::patch('/transaction/{transaction_id}', 'TransactionController@update')->name('update_transac');


Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');



Route::get('/get_uniqueid', 'GenerateIdController@getUniqueId');


Route::get('/stream/file/{file}', 'OrderController@stream_file');

//Route::get('user/show-items', 'OrderController@show_user_order_items')->name('show_user_order_items');


Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
});
/**
Auth routes begin from here
 **/
Route::group(['middleware' => 'auth:api'], function(){

    Route::get('user', 'AuthController@user');

    Route::get('profile', 'ProfileController@profile')->name('view_profile');

    Route::put('profile', 'ProfileController@updateProfile')->name('update_profile');



        Route::get('/store/show', 'StoreController@show')->name('show_store');
        Route::post('/store/update', 'StoreController@update')->name('update_store');

       //Route::delete('/store/delete/{store_id}', 'StoreController@delete')->name('delete_store');

    Route::get('/stores', 'StoreController@index')->name('index_store');
    Route::get('/store/products', 'StoreController@store_products')->name('store_products');
    Route::post('/store/create', 'StoreController@create_store')->name('create_store');
    Route::get('/store/orders/success', 'StoreController@storeSuccessOrder')->name('storeSuccessOrder');

    Route::group(['prefix'=>'order'],function (){

        Route::post('create', 'OrderController@save_order')->name('save_order');
        Route::get('all', 'OrderController@orders')->name('index_order');
        Route::get('user/show', 'OrderController@show_user_orders')->name('show_user_orders');
        Route::get('user/show-items', 'OrderController@show_user_order_items')->name('show_user_order_items');
        Route::post('view', 'OrderController@show_order')->name('show_order');

    });

    Route::post('/product/store', 'ProductController@store_product')->name('store_product');


    Route::get('/payment/callback', 'PaymentController@confirm_payment');


});

