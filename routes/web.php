<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{path?}', 'app');
Route::view('/product/{path?}', 'app');
Route::view('/payment/{path?}', 'app');
Route::view('/user/{path?}', 'app');
Route::view('/owner/store/{path?}', 'userstore');
Route::view('/item/edit/{path?}', 'app');



