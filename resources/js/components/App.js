import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom'
import { Route } from 'react-router'
import IndexComponent from './home/IndexComponent'
import SingleProductComponent from "./product/SingleProductComponent";
import CartComponent from "./product/CartComponent";
import Login from "./auth/Login";
import CheckoutComponent from "./product/CheckoutComponent";
import PaymentComponent from "./product/PaymentComponent";
import SuccessComponent from "./product/SuccessComponent";
import ProfileComponent from "./user/profile/ProfileComponent";
import EditFileComponent from "./user/product/EditFileComponent";
import UserStoreComponent from "./user/store/dashboard/UserStoreComponent";
import AddProduct from "./user/store/product/AddProduct";
import StoreProfile from "./user/store/profile/StoreProfile";
import AllProduct from "./user/store/product/AllProduct";
import AllSales from "./user/store/product/AllSales";


export default class App extends Component {

    constructor(props){
        super(props);

      //  this.checkAuth.bind(this);

        this.state = {
            data:"false",
            auththe: "false",
            authenticated:false,
            token:"",
            routes: [],
            sideBarRoutes: [],
            user:[],
            active: "",
        }
    }



    componentDidMount(){



        this.checkAuth();


    }



    checkAuth = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));



        if(storagedata != null && storagedata!=""){



            var auth = true;



        }else{

            var auth = false;



        }



        return auth



    }




    render() {




        return (


            <BrowserRouter>

                <Route exact path ="/" render = { (routeProps) => ( <IndexComponent {...routeProps} auth={this.checkAuth()} /> )} />

                <Route path="/product/:productId" render = { (routeProps) => ( <SingleProductComponent {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/cart" render = { (routeProps) => ( <CartComponent {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/checkout" render = { (routeProps) => ( <CheckoutComponent {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/payment/:orderId" render = { (routeProps) => ( <PaymentComponent {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/success" render = { (routeProps) => ( <SuccessComponent {...routeProps} auth={this.checkAuth()} /> )}  />


                <Route path="/user/profile" render = { (routeProps) => ( <ProfileComponent {...routeProps} auth={this.checkAuth()} /> )}  />

                <Route path="/item/edit/:itemId" render = { (routeProps) => ( <EditFileComponent {...routeProps} auth={this.checkAuth()} /> )}  />



                <Route path="/owner/store/dashboard" render = { (routeProps) => ( <UserStoreComponent {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/owner/store/addproduct" render = { (routeProps) => ( <AddProduct {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/owner/store/profile" render = { (routeProps) => ( <StoreProfile {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/owner/store/products" render = { (routeProps) => ( <AllProduct {...routeProps} auth={this.checkAuth()} /> )}  />
                <Route path="/owner/store/sales" render = { (routeProps) => ( <AllSales {...routeProps} auth={this.checkAuth()} /> )}  />



                <Route path="/login" component={Login} />







            </BrowserRouter>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
