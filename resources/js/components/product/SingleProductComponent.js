import React, {Component} from "react";
import Header from "../includes/Header";
import NProgress from "nprogress"

class SingleProductComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 0
        }
    }


    componentDidMount(){


        NProgress.start();

        const { match: { params } } = this.props;

        axios.get(`/api/product/${params.productId}`,
        ).then((response) => {

            console.log(response.data.data.length )

            if(response.data.data.length > 0) {

                NProgress.done();
                this.setState({data: response.data.data,httpstate:'2'})
            }else{
                NProgress.done();
                this.setState({data: response.data.data,httpstate:'2'})
            }


        })
    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                </div>
                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="container">

                        <div className="row">
                            <div className="col-md-12">
                                <div className="slider-wrapper">
                                </div>

                                <div className="empty-space col-xs-b20 col-sm-b35 col-md-b70"></div>

                                <div className="spacing-1">
                                    <div className="h4">Product Details</div>
                                </div>


                                <div className="empty-space col-xs-b25 col-sm-b60"></div>

                                <div className="products-content">
                                    <div className="products-wrapper">
                                        <div className="row">
                                            <div className="col-sm-5 col-xs-b30 col-sm-b0">

                                                <div className="main-product-slider-wrapper swipers-couple-wrapper">
                                                    <div className="swiper-container swiper-control-top">
                                                        <div className="swiper-button-prev hidden"></div>
                                                        <div className="swiper-button-next hidden"></div>
                                                        <div className="swiper-wrapper">

                                                            <div className="swiper-slide">
                                                                <div className="swiper-lazy-preloader"></div>
                                                                <div className="product-big-preview-entry swiper-lazy"
                                                                     data-background={this.state.data.image}></div>
                                                            </div>

                                                            {this.state.data.product_images.map((item,index) => {


                                                                return <div key={index} className="swiper-slide">
                                                                    <div className="swiper-lazy-preloader"></div>
                                                                    <div
                                                                        className="product-big-preview-entry swiper-lazy"
                                                                        data-background={item.path}></div>
                                                                </div>

                                                                })
                                                            }

                                                        </div>
                                                    </div>

                                                    <div className="empty-space col-xs-b30 col-sm-b60"></div>

                                                    <div className="swiper-container swiper-control-bottom"
                                                         data-breakpoints="1"
                                                         data-xs-slides="3" data-sm-slides="3" data-md-slides="4"
                                                         data-lt-slides="5"
                                                         data-slides-per-view="5" data-center="1" data-click="1">
                                                        <div className="swiper-button-prev hidden"></div>
                                                        <div className="swiper-button-next hidden"></div>
                                                        <div className="swiper-wrapper">
                                                            <div className="swiper-slide">
                                                                <div className="product-small-preview-entry">
                                                                    <img src={this.state.data.image}
                                                                         alt="" height="70px"
                                                                         width="70px"/>
                                                                </div>
                                                            </div>

                                                            {this.state.data.product_images.map((item,index) => {
                                                                return <div key={index} className="swiper-slide">
                                                                    <div className="product-small-preview-entry">
                                                                        <img src={item.path}
                                                                             alt="" height="70px"
                                                                             width="70px"/>
                                                                    </div>
                                                                </div>

                                                                })
                                                            }

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className="col-sm-6">
                                                <div
                                                    className="simple-article size-3 grey col-xs-b5">{this.state.data.category_name} </div>
                                                <div className="h3 col-xs-b25">{this.state.data.title}</div>
                                                <div className="row col-xs-b25">
                                                    <div className="col-sm-6">
                                                        <div className="simple-article size-5 grey">PRICE: <span
                                                            className="color">${this.state.data.price}.00</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 col-sm-text-right">
                                                        <div className="rate-wrapper align-inline">
                                                            <i className="fa fa-star" aria-hidden="true"></i>
                                                            <i className="fa fa-star" aria-hidden="true"></i>
                                                            <i className="fa fa-star" aria-hidden="true"></i>
                                                            <i className="fa fa-star" aria-hidden="true"></i>
                                                            <i className="fa fa-star-o" aria-hidden="true"></i>
                                                        </div>
                                                        <div className="simple-article size-2 align-inline">128
                                                            Reviews
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">

                                                    <div className="col-sm-6">
                                                        <div className="simple-article size-3 col-xs-b20">AVAILABLE.:
                                                            <span
                                                                className="grey">{(this.state.status ? "YES" : "NO")}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div
                                                    className="simple-article size-3 col-xs-b30">{this.state.data.description}</div>


                                                <div className="row col-xs-b40">
                                                    <div className="col-sm-3">
                                                        <div className="h6 detail-data-title size-1">quantity:</div>
                                                    </div>
                                                    <div className="col-sm-9">
                                                        <div className="quantity-select">
                                                            <span className="minus"></span>
                                                            <span className="number">1</span>
                                                            <span className="plus"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row col-xs-b40">
                                                    <label className="checkbox-entry">
                                                        <input
                                                            type="checkbox"/><span><b><em><strong>NOTE: </strong></em></b> Check if you
                                        want to customize (<b>#500</b> per page)</span>
                                                    </label>
                                                </div>
                                                <div className="row m5 col-xs-b40">
                                                    <div className="col-sm-6 col-xs-b10 col-sm-b0">
                                                        <a className="button size-2 style-2 block" href="#">
                                        <span className="button-wrapper">
                                            <span className="icon"><img src="img/icon-2.png" alt=""/></span>
                                            <span className="text">add to cart</span>
                                        </span>
                                                        </a>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <a className="button size-2 style-1 block noshadow" href="#">
                                        <span className="button-wrapper">
                                            <span className="icon"><i className="fa fa-heart-o" aria-hidden="true"></i></span>
                                            <span className="text">add to favourites</span>
                                        </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-3">
                                                        <div className="h6 detail-data-title size-2">share:</div>
                                                    </div>
                                                    <div className="col-sm-9">
                                                        <div className="follow light">
                                                            <a className="entry" href="#"><i
                                                                className="fa fa-facebook"></i></a>
                                                            <a className="entry" href="#"><i
                                                                className="fa fa-twitter"></i></a>
                                                            <a className="entry" href="#"><i
                                                                className="fa fa-linkedin"></i></a>
                                                            <a className="entry" href="#"><i
                                                                className="fa fa-google-plus"></i></a>
                                                            <a className="entry" href="#"><i
                                                                className="fa fa-pinterest-p"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="empty-space col-xs-b35 col-sm-b0"></div>

                                <div className="empty-space col-xs-b35 col-md-b70"></div>
                                <div className="empty-space col-md-b70"></div>
                            </div>

                        </div>


                    </div>

                </div>
            )

        }


        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </div>
                </React.Fragment>

            )
        }
    }
}


export default SingleProductComponent