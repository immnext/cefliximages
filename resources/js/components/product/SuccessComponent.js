import React, {Component} from "react";
import Header from "../includes/Header";
import Footer from "../includes/Footer";
import {Link} from "react-router-dom"
import NProgress from "nprogress"

class SuccessComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 2,
            cartItems:0,
            cartprice:0,
            token:'',
            reference:''
        }


    }


    componentDidMount(){

        NProgress.start();
        NProgress.done();

        this.checkAuth();

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        const query = new URLSearchParams(this.props.location.search);
        const ref = query.get('reference')

        this.setState({'reference':ref})
        


    }
    

    checkAuth(){


        console.log(this.props.auth);

        if(this.props.auth) {

            let storagedata = JSON.parse(localStorage.getItem('authuser'));

            console.log(storagedata)

            this.setState({
                token:storagedata.access_token,
            })

        }else{

            return  window.location.href= '/login?ref=checkout';

        }


    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                    <Footer />
                </div>

                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="container">
                        <div className="container">
                            <div className="empty-space col-xs-b15 col-sm-b30"></div>
                            <div className="breadcrumbs">
                                <Link href="#">home</Link>
                                <Link href="#">checkout</Link>
                                <Link href="#">success</Link>
                            </div>
                            <div className="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
                            <div className="text-center">
                                <div className="simple-article size-3 grey uppercase col-xs-b5">your order has been successfully processed</div>
                                <div className="h2">Thank You for buying</div>
                                <div className="title-underline center"><span></span></div>
                                <div className="row">
                                    <div className="col-md-8 col-md-offset-2">
                                        <div className="simple-article size-4 grey">Your order was successful, you can check your profile to view all your orders and their status</div>
                                        <div className="empty-space col-xs-b20 col-sm-b35 col-md-b70"></div>
                                        <div className="row m10">
                                            <div className="col-sm-12">
                                                Order ID <div className="empty-space col-xs-b15 col-sm-b30"></div>
                                                <div className="simple-input">{this.state.reference}</div>
                                                    <div className="empty-space col-xs-b20"></div>
                                            </div>

                                        </div>
                                        <Link to="/user/profile">
                                        <div className="button size-2 style-3">
                            <span className="button-wrapper">
                                <span className="icon"><img src="img/icon-4.png" alt=""/></span>
                                <span className="text">My orders</span>
                            </span>
                                            <input type="submit"/>
                                        </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                  

                    <div className="empty-space col-xs-b15 col-sm-b30"></div>
                    <div className="empty-space col-xs-b15 col-sm-b30"></div>
                    <Footer />

                </div>
            )

            }

        

        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                    <Footer />
                </div>
                </React.Fragment>

            )
        }
    }
}


export default SuccessComponent