import React, {Component} from "react";
import Header from "../includes/Header";
import { Link,Redirect } from 'react-router-dom'
import Footer from '../includes/Footer'
import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'

class CheckoutComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 0,
            cartItems:0,
            cartprice:0,
            token:"",


            country:"",
            first_name :"",
            last_name :"",
            country :"",
            street_address :"",
            city :"",
            state :"",
            zip_code :"",
            email :"",
            phone :"",
            products :"",
            active:0
        }
    }


    handleChange = (e) => {

        this.setState({

            [e.target.name]:e.target.value

        })

    }


    handleSubmit  = (event) => {

        event.preventDefault();

        const MySwal = withReactContent(Swal)

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+this.state.token,
        }


        let cartdata = JSON.parse(localStorage.getItem('cart'));

        if (cartdata != null) {

            if (cartdata.length > 0) {
                this.setState({data: cartdata})


                let price = 0;
                cartdata.map((item) => {
                    price = price + Number(item.price)
                })


                this.setState({cartprice: price})

                NProgress.start();


                axios.post('/api/order/create', {

                    email: this.state.email,
                    first_name: this.state.first_name,
                    last_name: this.state.last_name,
                    country: this.state.country,
                    street_address: this.state.street_address,
                    city: this.state.city,
                    state: this.state.state,
                    zip_code: this.state.zip_code,
                    email: this.state.email,
                    phone: this.state.phone,
                    products:this.state.products

                }, {
                    headers: headers
                })
                    .then((response) => {

                        if (response.data.status) {

                            NProgress.done();
                            window.location.href='/payment/'+response.data.order_id

                        } else {


                            MySwal.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: response.data.message,

                            })


                            this.setState({errormessage: response.data.message, messagetype: response.data.type})

                        }

                    })
                    .catch(function (error) {
                        console.log(error);
                    });

             }

         }

    }







    componentDidMount(){






        this.checkAuth();

        this.getcart()


    }

    checkAuth(){


        console.log(this.props.auth);

        if(this.props.auth) {

            let storagedata = JSON.parse(localStorage.getItem('authuser'));

            this.setState({
                token:storagedata.access_token,
            })

        }else{

            return  window.location.href= '/login?ref=checkout';

        }


    }



    getcart() {

        let cartdata = JSON.parse(localStorage.getItem('cart'));

        let products = [];

        if (cartdata != null){


            if (cartdata.length > 0) {
                this.setState({data: cartdata,httpstate:'2'})


                let price = 0;
                cartdata.map((item) => {
                    price  = price + Number(item.price);
                    products.push(item.id)

                })

                const product_arr = products.join(',')

                this.setState({products:product_arr})
                this.setState({cartprice: price})


            }

        }
    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                </div>
                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="container">
                        <div className="empty-space col-xs-b15 col-sm-b30"></div>
                        <div className="breadcrumbs">
                            <Link href="#">home</Link>
                            <Link href="#">shopping cart</Link>
                        </div>

                        <div className="text-center">
                            <div className="simple-article size-3 grey uppercase col-xs-b5">shopping cart</div>
                            <div className="h2">check your products</div>
                            <div className="title-underline center"><span></span></div>
                        </div>
                    </div>

                    <div className="container">
                        <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="col-md-6 col-xs-b50 col-md-b0">
                                <h4 className="h4 col-xs-b25">billing details</h4>
                                <select className="form-control" name="country" value={this.state.country} onChange={this.handleChange} required>
                                    <option value="">Choose country</option>
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="mercedes">Mercedes</option>
                                    <option value="audi">Audi</option>
                                </select>
                                <div className="empty-space col-xs-b20"></div>
                                <div className="row m10">
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="First name" required name="first_name" value={this.state.first_name} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="Last name" required name="last_name" value={this.state.last_name} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                </div>
                                <div className="row m10">
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="Email" required name="email" value={this.state.email} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="Phone" required name="phone" value={this.state.phone} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                </div>

                                <div className="empty-space col-xs-b20"></div>

                                <div className="empty-space col-xs-b20"></div>
                                <div className="row m10">
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="Street address" required name="street_address" value={this.state.street_address} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>

                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="city" required name="city" value={this.state.city} onChange={this.handleChange}/>
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>

                                </div>
                                <div className="row m10">
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="State" required name="state" value={this.state.state} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                    <div className="col-sm-6">
                                        <input className="simple-input" type="text"  placeholder="Postcode/ZIP" required name="zip_code" value={this.state.zip_code} onChange={this.handleChange} />
                                        <div className="empty-space col-xs-b20"></div>
                                    </div>
                                </div>

                                <label className="checkbox-entry">
                                    <input type="checkbox" checked /><span>Privacy policy agreement</span>
                                </label>


                                <div className="checkbox-toggle-wrapper">
                                    <div className="empty-space col-xs-b25"></div>
                                    <select className="SlectBox">
                                        <option disabled="disabled" >Choose country</option>
                                        <option value="volvo">Volvo</option>
                                        <option value="saab">Saab</option>
                                        <option value="mercedes">Mercedes</option>
                                        <option value="audi">Audi</option>
                                    </select>
                                    <div className="empty-space col-xs-b20"></div>
                                    <div className="row m10">
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="First name" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="Last name" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                    <input className="simple-input" type="text"  placeholder="Company name" />
                                    <div className="empty-space col-xs-b20"></div>
                                    <input className="simple-input" type="text"  placeholder="Street address" />
                                    <div className="empty-space col-xs-b20"></div>
                                    <div className="row m10">
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="Appartment" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="Town/City" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                    <div className="row m10">
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="State/Country" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                        <div className="col-sm-6">
                                            <input className="simple-input" type="text"  placeholder="Postcode/ZIP" />
                                            <div className="empty-space col-xs-b20"></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="empty-space col-xs-b30 col-sm-b60"></div>
                                <textarea className="simple-input" placeholder="Note about your order"></textarea>
                            </div>
                            <div className="col-md-6">
                                <h4 className="h4 col-xs-b25">your order</h4>

                                { this.state.data.map((item, index)=> {
                                    return <div key={index} className="cart-entry clearfix">
                                    <Link className="cart-entry-thumbnail" href="#"><img src={item.path} alt="" style={{width:"100px"}}/></Link>
                                    <div className="cart-entry-description">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div className="h6"><Link href="#">{item.title}</Link></div>

                                                </td>
                                                <td>
                                                    <div className="simple-article size-3 grey">${item.price}.00</div>
                                                    <div className="simple-article size-1">TOTAL: ${item.price}.00</div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                })
                                }






                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            cart subtotal
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">${this.state.cartprice}.00</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            order total
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">${this.state.cartprice}.00</div>
                                        </div>
                                    </div>
                                </div>


                                <div className="empty-space col-xs-b10"></div>
                                <div className="simple-article size-2">* You will not be redirected to a confirmation page after clicking the button below and you will not yet be billed.</div>
                                <div className="empty-space col-xs-b30"></div>
                                <div className="button block size-2 style-3">
                        <span className="button-wrapper">
                            <span className="icon"><img src="img/icon-4.png" alt="" /></span>
                            <span className="text">Proceed to Payment</span>
                        </span>
                                    <input type="submit"/>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                    <div className="empty-space col-xs-b20"></div>
                    <div className="empty-space col-xs-b20"></div>

                    <Footer />

                </div>
            )

            }




        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </div>
                </React.Fragment>

            )
        }
    }
}



export default CheckoutComponent