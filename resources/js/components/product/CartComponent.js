import React, {Component} from "react";
import Header from "../includes/Header";
import {Link} from "react-router-dom"

class CartComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 0,
            cartItems:0,
            cartprice:0
        }
    }


    componentDidMount(){

        this.getcart()


        // const { match: { params } } = this.props;
        //
        // axios.get(`/api/product/${params.productId}`,
        // ).then((response) => {
        //
        //     console.log(response.data.data.length )
        //
        //     if(response.data.data.length > 0) {
        //
        //         this.setState({data: response.data.data,httpstate:'2'})
        //     }else{
        //         this.setState({data: response.data.data,httpstate:'2'})
        //     }
        //
        //
        // })
    }



    getcart() {

        let cartdata = JSON.parse(localStorage.getItem('cart'));

        if (cartdata != null){



            if (cartdata.length > 0) {
                this.setState({data: cartdata,httpstate:'2'})


                let price = 0;
                cartdata.map((item) => {
                    price  = price + Number(item.price)
                })

                console.log(price);
                this.setState({cartprice: price})
            }

        }
    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                </div>
                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="container">
                        <div className="empty-space col-xs-b15 col-sm-b30"></div>
                        <div className="breadcrumbs">
                            <a href="#">home</a>
                            <a href="#">shopping cart</a>
                        </div>
                        <div className="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
                        <div className="text-center">
                            <div className="simple-article size-3 grey uppercase col-xs-b5">shopping cart</div>
                            <div className="h2">check your products</div>
                            <div className="title-underline center"><span></span></div>
                        </div>
                    </div>

                    <div className="container">
                        <table className="cart-table">
                            <thead>
                            <tr>
                                <th ></th>
                                <th>product name</th>
                                <th >price</th>
                                <th>total</th>
                                <th ></th>
                            </tr>
                            </thead>
                            <tbody>

                            { this.state.data.map((item, index)=> {
                               return  <tr key={index}>
                                    <td data-title=" ">
                                        <a className="cart-entry-thumbnail" href="#"><img src={item.path} alt="" style={{width:"100px"}}/></a>
                                    </td>
                                    <td data-title=" "><h6 className="h6"><a href="#">{item.title}</a></h6></td>
                                    <td data-title="Price: ">${item.price}.00</td>


                                    <td data-title="Total:">${item.price}.00</td>
                                    <td data-title="">
                                        <div className="button-close"></div>
                                    </td>
                                </tr>
                             })
                            }



                            </tbody>
                        </table>
                        <div className="empty-space col-xs-b35"></div>
                        <div className="row">
                            <div className="col-sm-6 col-md-5 col-xs-b10 col-sm-b0"></div>
                            <div className="col-sm-6 col-md-7 col-sm-text-right">
                                <div className="buttons-wrapper">

                                    <Link className="button size-2 style-3" to="/checkout">
                            <span className="button-wrapper">
                                <span className="icon"><img src="img/icon-4.png" alt=""/></span>
                                <span className="text">proceed to checkout</span>
                            </span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="empty-space col-xs-b35 col-md-b70"></div>
                        <div className="row">
                            <div className="col-md-6 col-xs-b50 col-md-b0"></div>
                            <div className="col-md-6">
                                <h4 className="h4">cart totals</h4>
                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            cart subtotal
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">${this.state.cartprice}.00</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            order total
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">${this.state.cartprice}.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="empty-space col-xs-b35 col-md-b70"></div>
                        <div className="empty-space col-xs-b35 col-md-b70"></div>
                    </div>

                </div>
            )

            }




        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </div>
                </React.Fragment>

            )
        }
    }
}


export default CartComponent