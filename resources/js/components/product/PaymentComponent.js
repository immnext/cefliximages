import React, {Component} from "react";
import Header from "../includes/Header";
import Footer from "../includes/Footer";
import {Link} from "react-router-dom"
import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'

class PaymentComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 0,
            cartItems:0,
            cartprice:0,
            token:''
        }


    }


    componentDidMount(){

        NProgress.start();

        this.checkAuth();

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        const { match: { params } } = this.props;

        axios.post(`/api/order/view`,

                {
                    order_id: `${params.orderId}`,
                },
                {
                    headers: headers
                }
            ).then((response) => {

                console.log(response.data.data.length )

                if(response.data.data.length > 0) {

                    NProgress.done();
                    this.setState({data: response.data.data,httpstate:'2'})
                }else{

                    NProgress.done();
                    this.setState({data: response.data.data,httpstate:'2'})
                }


            })



    }

    logout() {

        localStorage.removeItem('authuser');
        window.location.href="/"

    }



    confirm = (reference) =>{


        NProgress.start();

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }

        const MySwal = withReactContent(Swal);

        $("#paybutton").hide();


        axios.get('/api/payment/callback?reference='+reference
,
            {
                headers: headers
            }
        ).then((response) => {


            if(response.status){

                localStorage.removeItem('cart');
                window.location.href="/success?reference="+reference

            }else{
                MySwal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: response.data.message,

                })
            }


        })


    }




     payWithPaystack = () =>{


         const MySwal = withReactContent(Swal);

        var handler = PaystackPop.setup({
            key: 'pk_test_150c834486ba274657c0bb15d188d6eafd6cdd9a',
            email: 'customer@email.com',
            amount: this.state.data.total_amount * 100,
            currency: "NGN",
            //ref: ''+Math.floor((Math.random() * 1000000000) + 1),
            ref: this.state.data.unique_id, // generates a pseudo-unique reference. Please replace with a
            // reference you generated. Or remove the line entirely so our API will generate one for you
            metadata: {
                custom_fields: [
                    {
                        display_name: "Mobile Number",
                        variable_name: "mobile_number",
                        value: "+2348012345678"
                    }
                ]
            },
            callback: (response) => {

                this.confirm(response.reference)
                //alert('success. transaction ref is ' + response.reference);

            },
            onClose: function(){
                alert('window closed');
            }
        });
        handler.openIframe();
}



    checkAuth(){




        console.log(this.props.auth);

        if(this.props.auth) {

            let storagedata = JSON.parse(localStorage.getItem('authuser'));

            console.log(storagedata)

            this.setState({
                token:storagedata.access_token,
            })

        }else{

            return  window.location.href= '/login?ref=checkout';

        }


    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                    <Footer />
                </div>

                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="container">
                        <div className="empty-space col-xs-b15 col-sm-b30"></div>
                        <div className="breadcrumbs">
                            <Link to="#">home</Link>
                            <Link to="#">shopping cart</Link>
                        </div>
                        <div className="empty-space col-xs-b15 col-sm-b50 col-md-b100"></div>
                        <div className="text-center">
                            <div className="simple-article size-3 grey uppercase col-xs-b5">shopping cart</div>
                            <div className="h2">check your products</div>
                            <div className="title-underline center"><span></span></div>
                        </div>
                    </div>

                    <div className="container">

                        <div className="row">
                            <div className="col-md-6 col-xs-b50 col-md-b0">


                                <h4 className="h4 col-xs-b25">your order</h4>

                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            Order Items count
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">{this.state.data.order_item.length}</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="order-details-entry simple-article size-3 grey uppercase">
                                    <div className="row">
                                        <div className="col-xs-6">
                                            order price
                                        </div>
                                        <div className="col-xs-6 col-xs-text-right">
                                            <div className="color">${this.state.data.total_amount}.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="empty-space col-xs-b50"></div>
                                <h4 className="h4 col-xs-b25">payment method</h4>

                                <div className="empty-space col-xs-b10"></div>
                                <div className="simple-article size-2">Please confirm order before clicking on the button.</div>
                                <div className="empty-space col-xs-b30"></div>

                                <div className="button block size-2 style-3" id="paybutton">
                        <span className="button-wrapper">
                            <span className="icon"><img src="img/icon-4.png" alt=""/></span>
                            <span className="text">Proceed to Pay</span>
                        </span>
                                    <input type="submit" onClick={this.payWithPaystack}/>
                                </div>



                            </div>



                            <div className="col-md-6">
                                <h4 className="h4 col-xs-b25">Order Items</h4>

                                { this.state.data.order_item.map((item,index) => {


                                    return <div key={index} className="cart-entry clearfix">
                                        <Link className="cart-entry-thumbnail" to="#"><img src={item.product_image}
                                                                                           alt="" style={{width:"100px"}}/></Link>
                                        <div className="cart-entry-description">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="h6"><Link to="#">{item.product_title}t</Link></div>

                                                    </td>
                                                    <td>
                                                        <div className="simple-article size-3 grey">${item.product_amount}.00</div>
                                                        <div className="simple-article size-1">TOTAL: ${item.product_amount}.00</div>
                                                    </td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                })

                                }




                                <div className="button block size-2 style-3"></div>
                            </div>
                        </div>

                    </div>

                    <div className="empty-space col-xs-b15 col-sm-b30"></div>
                    <div className="empty-space col-xs-b15 col-sm-b30"></div>


                    <div class="modal hide fade" id="myModal">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>Modal header</h3>
                        </div>
                        <div class="modal-body">
                            <p>One fine body…</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn">Close</a>
                            <a href="#" class="btn btn-primary">Save changes</a>
                        </div>
                    </div>
                    <Footer />

                </div>
            )

            }

        

        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                    <Footer />
                </div>
                </React.Fragment>

            )
        }
    }
}


export default PaymentComponent