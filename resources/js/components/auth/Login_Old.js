import React,{ Component } from 'react'
import { Link } from 'react-router-dom'



class Login_Old extends Component{

    constructor(props){
        super(props);

        this.state ={
            email:"elda12@example.net",
            password:"password",
            messagetype:"",
            errormessage:"",
            errormessages:[

            ],



        };
    }


    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }




    handleSubmit  = (event) => {

        event.preventDefault();

        const headers = {
            'Accept': 'application/json',
        }


         $(".login-page").addClass('logging-in');

         console.log(this.state.email)


         axios.post('/api/auth/login', {
             email: this.state.email,
             password: this.state.password
         },{
             headers: headers
         })
             .then( (response) => {

                 if(response.data.status){

                   //  console.log(response.data)

                     const datar = {
                         status:response.data.status,
                         access_token:response.data.access_token,
                         expires_at:response.data.expires_at,
                         user:response.data.user
                     }

                     localStorage.setItem('authuser',JSON.stringify(datar));

                     window.location.href="/dashboard"


                 }else{

                     $(".login-page").removeClass('logging-in');
                     this.setState({errormessage:response.data.message,messagetype:response.data.type})

                 }

             })
             .catch(function (error) {
                 console.log(error);
             });



     }

     componentDidMount () {


     }


    render (){

        return (

            <div className="page-body login-page login-form-fall" style={{background: "#062a52"}}>
            <div className="login-container">
                <div className="login-header login-caret" style={{"background": "#053d7d"}}>
                    <div className="login-content"><Link to="/dashboard/main/index.html" className="logo"> <img
                        src="assets/images/logo@2x.png" width="120" alt=""/> </Link>
                        <p className="description">Dear users, log in to access the admin area!</p>

                    </div>


                </div>


                <div className="login-progressbar">
                    <div></div>
                </div>
                <div className="login-form">
                    <div align="center" className="col-md-6 offset-3">
                        <div className={'alert alert-'+this.state.messagetype}>
                            {this.state.errormessage}
                        </div>
                    </div>
                    <div className="login-content">
                        <div className="form-login-error"><h3>Invalid login</h3>
                            <p>Enter <strong>demo</strong>/<strong>demo</strong> as login and password.</p></div>
                        <form method="post" role="form" id="form_login" onSubmit={this.handleSubmit}>
                            <div className="form-group">
                                <div className="input-group">
                                    <div className="input-group-addon"><i className="entypo-user"></i></div>
                                    <input type="text"  className="form-control" name="username" id="email" placeholder="Username"
                                           autoComplete="off" value={this.state.email} onChange={this.handleChange} required/></div>
                            </div>
                            <div className="form-group">
                                <div className="input-group">
                                    <div className="input-group-addon"><i className="entypo-key"></i></div>
                                    <input type="password" className="form-control" name="password" id="password" placeholder="Password"
                                           autoComplete="off" value={this.state.password} onChange={this.handleChange} required/></div>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary btn-block btn-login"><i className="entypo-login"></i>
                                    Login In
                                </button>
                            </div>
                            <div className="form-group"><em>- or -</em></div>
                            <div className="form-group">
                                <button type="button" className="btn btn-default btn-lg btn-block btn-icon icon-left facebook-button">
                                    Login with Facebook
                                    <i className="entypo-facebook"></i></button>
                            </div>
                        </form>
                        <div className="login-bottom-links"><Link to="../forgot-password/index.html" className="link">Forgot your
                            password?</Link> <br/> <Link to="index.html#">ToS</Link> - <Link to="index.html#">Privacy Policy</Link></div>
                    </div>
                </div>
            </div>
            </div>

        );
    }
}



export default Login_Old;