import React,{ Component } from 'react'
import { Link } from 'react-router-dom'



class Login extends Component{

    constructor(props){
        super(props);

        this.state ={
            email:"bonita.johnston@example.org",
            password:"password",
            messagetype:"",
            errormessage:"",
            errormessages:[

            ],



        };
    }


    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }




    handleSubmit  = (event) => {

        event.preventDefault();

        const headers = {
            'Accept': 'application/json',
        }







         axios.post('/api/auth/login', {
             email: this.state.email,
             password: this.state.password
         },{
             headers: headers
         })
             .then( (response) => {

                 if(response.data.status){

                   //  console.log(response.data)

                     const datar = {
                         status:response.data.status,
                         access_token:response.data.access_token,
                         expires_at:response.data.expires_at,
                         user:response.data.user
                     }

                     localStorage.setItem('authuser',JSON.stringify(datar));

                     window.location.href="/dashboard"


                 }else{

                     $(".login-page").removeClass('logging-in');
                     this.setState({errormessage:response.data.message,messagetype:response.data.type})

                 }

             })
             .catch(function (error) {
                 console.log(error);
             });



     }

     componentDidMount () {


     }


    render (){

        return (





                    <div className="content">
                    <div className="row">
                        <div className="col-md-4 offset-3">


                            <div className="card">
                                <div className="alert alert-primary">
                                    <div align="center">
                                        {this.state.errormessage}
                                    </div>

                                    <p className="card-category  text-center">Welcome to Real Estate</p>
                                </div>

                                <div className="card-body">
                                    <div align="center"><img src="/assets/images/logo_dark.png" /></div> <br />
                                    <form onSubmit={this.handleSubmit}>

                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group bmd-form-group">
                                                    <label className="bmd-label-floating">Email</label>
                                                    <input type="text" id="email" className="form-control" value={this.state.email} onChange={this.handleChange} required/>
                                                </div>
                                            </div>
                                            <div className="col-md-12">
                                                <div className="form-group bmd-form-group">
                                                    <label className="bmd-label-floating">Password </label>
                                                    <input id="password" type="password" className="form-control" value={this.state.password} onChange={this.handleChange} required />
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" className="btn btn-primary pull-right">Login</button>
                                        <div className="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                    </div>


        );
    }
}



export default Login;