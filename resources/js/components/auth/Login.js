import React, {Component} from "react";
import Header from "../includes/Header";
import { Link } from 'react-router-dom'

class Login extends Component{


    constructor(props){
        super(props)

        this.state ={
            email:"",
            password:"",
            messagetype:"",
            errormessage:"",
            errormessages:[

            ],

        };
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }




    handleSubmit  = (event) => {

        event.preventDefault();

        const headers = {
            'Accept': 'application/json',
        }


        $(".login-page").addClass('logging-in');

        console.log(this.state.email)


        axios.post('/api/auth/login', {
            email: this.state.email,
            password: this.state.password
        },{
            headers: headers
        })
            .then( (response) => {

                if(response.data.status){

                    //  console.log(response.data)

                    const datar = {
                        status:response.data.status,
                        access_token:response.data.access_token,
                        expires_at:response.data.expires_at,
                        user:response.data.user
                    }

                    localStorage.setItem('authuser',JSON.stringify(datar));

                    const query = new URLSearchParams(this.props.location.search);
                    const ref = query.get('ref')

                    if(ref!="" && ref!=null){
                       /// console.log(ref)
                        window.location.href=ref
                    }else{
                        window.location.href="/"
                    }


                }else{


                    this.setState({errormessage:response.data.message,messagetype:response.data.type})

                }

            })
            .catch(function (error) {
                console.log(error);
            });

    }


    componentDidMount(){

        const query = new URLSearchParams(this.props.location.search);
        const ref = query.get('ref')

        console.log(ref);
    }





    render (){



            return (
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>

                    <div className="grey-background">
                        <div className="empty-space col-xs-b40 col-sm-b80"></div>
                        <div className="container">
                            <div className="row">

                                <div className="col-md-12">
                                    <div align="center" className="col-md-6 offset-3">
                                        <div className={'alert alert-'+this.state.messagetype}>
                                            {this.state.errormessage}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <img src="/img/abby-beauty-saloon-550x550.jpg" alt="" />
                            </div>
                            <div className="col-md-6">

                                <div className="product-shortcode style-3">

                                    <form onSubmit={this.handleSubmit} action="submit">

                                    <h3 className="h3 text-center">Log in</h3>
                                    <div className="empty-space col-xs-b30"></div>
                                    <input className="simple-input" type="text" id="email"  placeholder="Your email"  required value={this.state.email} onChange={this.handleChange}/>
                                    <div className="empty-space col-xs-b10 col-sm-b20"></div>
                                    <input className="simple-input" type="password" id="password" value={this.state.password} onChange={this.handleChange} placeholder="Enter password"  required />
                                    <div className="empty-space col-xs-b10 col-sm-b20"></div>
                                    <div className="row">
                                        <div className="col-sm-6 col-xs-b10 col-sm-b0">
                                            <div className="empty-space col-sm-b5"></div>
                                            <Link className="simple-link" href="#" >Forgot password?</Link>
                                            <div className="empty-space col-xs-b5"></div>
                                            <Link className="simple-link" href="register.html">register now</Link>
                                        </div>
                                        <div className="col-sm-6 text-right">

                                        <button className="button-wrapper" type="submit">
                                            <span className="icon"><img src="img/icon-4.png" alt="" /></span>
                                            <span className="text">submit</span>
                                        </button>

                                        </div>
                                    </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                </div>

                
            )





    }
}


export default Login