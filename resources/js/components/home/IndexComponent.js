import React,{Component} from "react"
import {Link} from 'react-router-dom'

//sweet alert begins
import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'
//sweet alert ends

import LatestProducts from "./LatestProducts";
import BestCategories from "./BestCategories";
import PopularCategories from "./PopularCategories";
import SideBanner from "./SideBanner";
import MostViewed from "./MostViewed";
import PeoplesChoice from "./PeoplesChoice";
import FeaturedProducts from "./FeaturedProducts";
import PromoProducts from "./PromoProducts";
import Sponsored from "./Sponsored";
import HomeSliders from "./HomeSliders";
import Footer from "../includes/Footer";
import Header from "../includes/Header";
import NewsLetter from "../includes/NewsLetter";


export default class IndexComponent extends Component {

    constructor(props){
        super(props)
    }


    addToCart(title,id,price,image){


        NProgress.start()
        const MySwal = withReactContent(Swal);

        let cartdata = JSON.parse(localStorage.getItem('cart'));

        if(cartdata != "" && cartdata !==null){


            var stored = JSON.parse(localStorage.getItem("cart"));

            var student2 = { id: id,itle:title,price:price,path:image };

            stored.push(student2);

            localStorage.setItem("cart", JSON.stringify(stored));

            var result = JSON.parse(localStorage.getItem("cart"));

            console.log("Existing cart updated")

        }else{

            var students = [];

            var student1 = { id: id,title:title,price:price,path:image };

            students.push(student1);

            localStorage.setItem("cart", JSON.stringify(students));
        }


        NProgress.done();

        MySwal.fire({

            type: 'success',
            title: 'Success',
            text: "Item Successfully added to cart",

        })

        // const datar = {
        //     id:response.data.status,
        // }
        //
        // localStorage.setItem('cart',JSON.stringify(datar));
    }


    addToCarterr(id){


        let cartdata = JSON.parse(localStorage.getItem('cart'));

        if(cartdata != ""){
            console.log("cart not exist or empty agansns"+id)
        }else{

            console.log("cart exist")
        }

        // const datar = {
        //     id:response.data.status,
        // }
        //
        // localStorage.setItem('cart',JSON.stringify(datar));
    }



    
    render(){
        
        return (
            <div id="content-block">
              
               <Header/>

                <div className="header-empty-space"></div>

               <HomeSliders/>

                <div className="grey-background">
                    <div className="empty-space col-xs-b40 col-sm-b80"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-9 col-md-push-3">

                                <LatestProducts addToCart = {this.addToCart} />

                                <BestCategories/>

                                <PeoplesChoice/>

                                <PromoProducts addToCart = {this.addToCart} />

                                <Sponsored addToCart = {this.addToCart} />


                            </div>

                            <div className="col-md-3 col-md-pull-9">


                                <PopularCategories/>


                                <div className="row">

                                        <SideBanner/>


                                        <MostViewed/>



                                </div>

                                <div className="row">

                                       <FeaturedProducts addToCart = {this.addToCart} />

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <NewsLetter/>

                <Footer/>
            </div>
        );
    }
}


