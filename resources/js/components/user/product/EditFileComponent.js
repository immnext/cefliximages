import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import NProgress from "nprogress"
import Header from "../../includes/Header";



class EditFileComponent extends  Component{
    constructor(props){
        super(props)

        this.state = {
            data: [],
            httpstate:'2',
            n:0
        }
    }


    componentDidMount(){

       document.getElementById('myIframe').style.height = 'calc(100vh - ' + ($('#myIframe').offset().top + 25) +'px)';


        NProgress.start();

        const { match: { params } } = this.props;

        axios.get(`/api/product/${params.itemId}`,
        ).then((response) => {



            if(response.data.data.length > 0) {

                NProgress.done();
                this.setState({data: response.data.data,httpstate:'2'})
            }else{
                NProgress.done();
                this.setState({data: response.data.data,httpstate:'2'})
            }


        })






    }




    getItems = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get('/api/order/user/show-items',

            {
                headers: headers
            })

            .then( (response) => {
                if(response.data.items.length > 0) {


                    this.setState({data: response.data.items,httpstate:'2'})
                }else{
                    this.setState({data: response.data.items,httpstate:'1'})
                }

                NProgress.done();
            })
    }


    encodeguy(){
        const timi = {
            "files" : [
                "/gpilogo.psd",
                "/gpilogo.png",
                "data:image/png;base64,iVBORw0KGgoAAAAN..."
            ],
            "resources" : [
                "https://www.xyz.com/brushes/Nature.ABR",
                "https://www.xyz.com/grads/Gradients.GRD",
                "https://www.xyz.com/fonts/NewFont.otf"
            ],
            "server" : {
                "version" : 1,
                "url"     : "https://www.myserver.com/saveImage.php",
                "formats" : [ "psd:true", "png", "jpg:0.5" ]
            },

            "script" : "app.activeDocument.rotateCanvas(90);"
        }

        return encodeURI(timi);
    }


    return_unique = () =>{

        return this.state.unique_id;
    }


    render() {

        if (this.state.httpstate == 0){
            return (
                <React.Fragment>
                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </React.Fragment>
            )
        }

        if(this.state.httpstate == 2){



            const dda =
            {
                "files" : [
                "https://www.mysite.com/images/design.psd",
                "https://www.mysite.com/images/button.png",
                "data:image/png;base64,iVBORw0KGgoAAAAN..."
            ],
                "resources" : [
                "https://www.xyz.com/brushes/Nature.ABR",
                "https://www.xyz.com/grads/Gradients.GRD",
                "https://www.xyz.com/fonts/NewFont.otf"
            ],
                "server" : {
                "version" : 1,
                    "url"     : "https://www.myserver.com/saveImage.php",
                    "formats" : [ "psd:true", "png", "jpg:0.5" ]
            },
                "script" : "app.activeDocument.rotateCanvas(90);"
            }




            const { match: { params } } = this.props;
    const ddd = params.itemId;
    const hostname = window.location.hostname;

    console.log(hostname)

        return (



            <React.Fragment>



                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>



                    <div className="containeddr">
                        <iframe id="myIframe" src={`https://www.photopea.com/#%7B%22files%22:%5B%22https://cefliximages.herokuapp.com/api/stream/file/${ddd}%22%5D%7D`} style={{width:"100%"}}>

                        </iframe>

                    </div>


                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">

                            </div>
                        </div>



                        <div className="empty-space col-xs-b35 col-md-b70"></div>
                        <div className="empty-space col-xs-b35 col-md-b70"></div>
                    </div>

                </div>




            </React.Fragment>



        )}

         if(this.state.httpstate == 1) {
             return (

                 <React.Fragment>
                     <div className="alert alert-warning">
                         No Item(s) found
                     </div>
                     <div className="empty-space col-xs-b35 col-md-b70"></div>
                 </React.Fragment>

             )
         }
    }
}

export default EditFileComponent;



