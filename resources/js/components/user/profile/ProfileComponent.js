import React, {Component} from "react";
import Header from "../../includes/Header";
import { Link,Redirect } from 'react-router-dom'
import Footer from '../../includes/Footer'
import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'
import OrdersComponent from "./OrdersComponent";
import OrderItemsComponent from "./OrderItemsComponent";

class ProfileComponent extends Component{


    constructor(props){
        super(props)

        this.state  = {
            'data':[],
            'httpstate': 2,
            cartItems:0,
            cartprice:0,
            token:"",


            country:"",
            first_name :"",
            last_name :"",
            country :"",
            street_address :"",
            city :"",
            state :"",
            zip_code :"",
            email :"",
            phone :"",
            products :"",
            active:0,
            store_name:"",
            store_description:""
        }
    }


    handleChange = (e) => {

        this.setState({

            [e.target.name]:e.target.value

        })

    }


    handleSubmit  = (event) => {

        event.preventDefault();

        const MySwal = withReactContent(Swal)
        NProgress.start();

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+this.state.token,
        }


                axios.post('/api/store/create', {

                    name: this.state.store_name,
                    description: this.state.store_description,

                }, {
                    headers: headers
                })
                    .then((response) => {

                        if (response.data.status) {

                            NProgress.done();
                            window.location.href='/owner/store/dashboard'

                        } else {


                            NProgress.done();

                            MySwal.fire({
                                type: 'error',
                                title: 'Oops...',
                                text: response.data.message,

                            })


                            this.setState({errormessage: response.data.message, messagetype: response.data.type})

                        }

                    })
                    .catch(function (error) {
                        console.log(error);
                    });



    }







    componentDidMount(){


        this.checkAuth();

        this.getStore();



    }

    checkAuth(){


        console.log(this.props.auth);

        if(this.props.auth) {

            let storagedata = JSON.parse(localStorage.getItem('authuser'));

            this.setState({
                token:storagedata.access_token,
            })

        }else{

            return  window.location.href= '/login?ref=/user/profile';

        }


    }

    userdata(){
        let storagedata = JSON.parse(localStorage.getItem('authuser'));
        return storagedata;
    }



    getcart() {

        let cartdata = JSON.parse(localStorage.getItem('cart'));

        let products = [];

        if (cartdata != null){


            if (cartdata.length > 0) {
                this.setState({data: cartdata,httpstate:'2'})


                let price = 0;
                cartdata.map((item) => {
                    price  = price + Number(item.price);
                    products.push(item.id)

                })

                const product_arr = products.join(',')

                this.setState({products:product_arr})
                this.setState({cartprice: price})


            }

        }
    }




    getStore = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }

        axios.get('/api/user', {
            headers: headers
        }).then((response) => {

                if (response.data.status) {

                   if(response.data.user.store_id != "" && response.data.user.store_id != null){
                       $("#enterstore").show(300)

                   }else{
                       $("#createstore").show(300)
                   }

                }

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render (){

        if (this.state.httpstate == 0){
            return ( <React.Fragment>


                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>


                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                </div>
                </React.Fragment>

            )
        }


        if (this.state.httpstate == 2) {
            return (
                <div id="content-block">

                    <Header/>
                    <div className="popup-wrapper">
                        <div className="bg-layer"></div>
                    <div className="popup-content" data-rel="1">
                        <div className="layer-close"></div>
                        <div className="popup-container size-1">
                            <div className="popup-align">
                                <h3 className="h3 text-center">Create Store</h3>
                                <div className="empty-space col-xs-b30"></div>
                                <input className="simple-input" type="text" onChange={this.handleChange} value={this.state.store_name} name="store_name"placeholder="Store Name" />
                                <div className="empty-space col-xs-b10 col-sm-b20"></div>
                                <textarea className="simple-input" onChange={this.handleChange} value={this.state.store_description} name="store_description" placeholder="About Store " ></textarea>

                                <div className="empty-space col-xs-b10 col-sm-b20"></div>
                                <input className="simple-checkbox" type="radio"/>I accept terms and conditions


                                <div className="empty-space col-xs-b10 col-sm-b20"></div>
                                <div className="row">

                                    <div className="col-sm-6 text-right">
                                        <div className="button block size-2 style-3">
                        <span className="button-wrapper">
                            <span className="icon"><img src="img/icon-4.png" alt=""/></span>
                            <span className="text">Submit</span>
                        </span>
                                            <input type="submit" onClick={this.handleSubmit}/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div className="button-close"></div>
                        </div>
                    </div>
                    </div>

                    <div className="header-empty-space"></div>

                    <div className="empty-space col-xs-b35 col-md-b70"></div>

                    <div className="container">
                        <div className="row">

                            <div className="col-sm-6">
                                <div className="swiper-slide">
                                    <div className="icon-description-shortcode style-2">
                                        <img className="image-icon image-thumbnail rounded-image" style=
                                            {{"height": "230px", width: "250px", "borderRadius": "50%"}} src={"/"+this.userdata().user.photo} alt="" />
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm-6" >
                                <div className="simple-article size-3 grey uppercase col-xs-b5">my profile</div>
                                <div className="h3">{this.userdata().user.first_name+" "+this.userdata().user.last_name}</div>
                                <div className="title-underline left"><span></span></div>
                                <div className="simple-article size-4 grey"><i className="fa fa-money fa-2x"></i>: 0.00</div>
                                <div className="title-underline left"><span></span></div>

                                <Link to="#" id="createstore" style={{display:"none"}}><div className="col-sm-12"> <div
                           className="open-popup" data-rel="1"> <div className="button size-2 style-3"> <span className="button-wrapper"> <span className="icon"><img src="img/icon-4.png" alt=""/></span> <span className="text">Create Store</span> </span>
                                     </div> </div> </div>
                                </Link>

                                <Link to="/owner/store/dashboard" target="_blank" id="enterstore" style={{display:"none"}}><div className="col-sm-12"> <div
                                    className=""> <div className="button size-2 style-3"> <span className="button-wrapper"> <span className="icon"><img src="img/icon-4.png" alt=""/></span> <span className="text">Enter Store</span> </span>
                                </div> </div> </div>
                                </Link>

                            </div>

                        </div>
                    </div>



                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                    <div className="card banner-shortcode style-6">
                        <div className="container">
                            <div className="h4 swiper-title">My Orders</div>

                            <div className="empty-space col-xs-b20"></div>

                            <OrdersComponent token={this.state.token}/>

                        </div>
                    </div>



                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                    <div className="" style={{padding:"50px 30px 50px 15px",background: "#fff"}}>
                        <div className="container">
                            <div className="slider-wrapper ">
                                <div className="swiper-button-prev hidden"></div>
                                <div className="swiper-button-next hidden"></div>
                                <div className="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3"  data-slides-per-view="3" data-space="30">
                                    <div className="h4 swiper-title">my order items</div>
                                    <div className="empty-space col-xs-b20"></div>

                                    <OrderItemsComponent/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="empty-space col-xs-b20"></div>
                    <div className="empty-space col-xs-b20"></div>



                    <Footer />

                </div>
            )

            }




        if(this.state.httpstate == 1) {
            return (  <React.Fragment>
                <div id="content-block">

                    <Header/>

                    <div className="header-empty-space"></div>
                    <div className="alert alert-warning">
                        No product found
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </div>
                </React.Fragment>

            )
        }
    }
}



export default ProfileComponent