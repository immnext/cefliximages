import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import NProgress from "nprogress"
import doten from "dotenv"


class DetailsComponent extends  Component{
    constructor(props){
        super(props)

        this.state = {
            data: [],
            httpstate:'0',
            store:''
        }
    }


    componentDidMount(){

        NProgress.start();
       this.getOrders();
    }


    getOrders = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));



        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get('/api/user/',

            {
                headers: headers
            })

            .then( (response) => {
                if(response.data.user.length > 0) {

                    this.setState({data: response.data.user,httpstate:'2'})
                }else{
                    this.setState({data: response.data.user,httpstate:'1'})
                }
            })
    }




    render() {

        if (this.state.httpstate == 0){
            return (
                <React.Fragment>
                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </React.Fragment>
            )
        }

        if(this.state.httpstate == 2){
        return (


            <React.Fragment>
                {(this.state.data.store==null && this.state.data.store=="" ?

                            <Link to="#"><div className="col-sm-12"> <div
                                className="open-popup" data-rel="1"> <div className="button size-2 style-3"> <span className="button-wrapper"> <span className="icon"><img src="img/icon-4.png" alt=""/></span> <span className="text">EnterEE Store</span> </span>
                            </div> </div> </div> </Link>


                        :

                        <div>I noget store</div>
                )}


            </React.Fragment>



        )}

         if(this.state.httpstate == 1) {
             return (
                 <React.Fragment>
                     <div className="alert alert-warning">
                         No order found
                     </div>
                     <div className="empty-space col-xs-b35 col-md-b70"></div>
                 </React.Fragment>
             )
         }
    }
}

const addStyle = {padding:"20px", fontWeight: "300"

}

export default DetailsComponent;



