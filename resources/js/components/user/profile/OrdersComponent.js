import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import NProgress from "nprogress"


class OrdersComponent extends  Component{
    constructor(props){
        super(props)

        this.state = {
            data: [],
            httpstate:'0'
        }
    }


    componentDidMount(){

        NProgress.start();
       this.getOrders();
    }



    getOrders = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));



        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get('/api/order/user/show',

            {
                headers: headers
            })

            .then( (response) => {
                if(response.data.data.length > 0) {

                    this.setState({data: response.data.data,httpstate:'2'})
                }else{
                    this.setState({data: response.data.data,httpstate:'1'})
                }
            })
    }




    render() {

        if (this.state.httpstate == 0){
            return (
                <React.Fragment>
                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </React.Fragment>
            )
        }

        if(this.state.httpstate == 2){
        return (


            <React.Fragment>

                <div className="table-responsive" style={{maxHeight:"500px",overflow:"scroll"}} >
                    <table className="table table-bordered table-striped table-hover" >
                        <thead>
                        <tr className="title h6">
                            <th style={addStyle}>Order Id</th>
                            <th style={addStyle}>No of Products</th>
                            <th style={addStyle}>Total Price</th>
                            <th style={addStyle}>Date</th>
                            <th style={addStyle}>Status</th>
                        </tr>
                        </thead>
                        <tbody>




                                    { this.state.data.map((item,index) => {

                                           return  <tr key={index} className="description simple-article size-3" style=
                                                       {{"font-size": "medium",padding:"20px !important;"}}>
                                                       <td style={addStyle}>{item.unique_id}</td>
                                                       <td style={addStyle}>11</td>
                                                       <td style={addStyle}>${item.total_amount}</td>
                                                       <td style={addStyle}>{item.created_at}</td>

                                                   {( item.status == 1
                                                           ?
                                                           <td style={addStyle} className="text-success">completed</td>
                                                           :
                                                           <td style={addStyle} className="text-danger">Not completed</td>

                                                   )}

                                                   </tr>

                                        })

                                    }

                        </tbody>
                    </table>
                </div>
                <div className="swiper-pagination relative-pagination visible-xs visible-sm"></div>




            </React.Fragment>



        )}

         if(this.state.httpstate == 1) {
             return (
                 <React.Fragment>
                     <div className="alert alert-warning">
                         No order found
                     </div>
                     <div className="empty-space col-xs-b35 col-md-b70"></div>
                 </React.Fragment>
             )
         }
    }
}

const addStyle = {padding:"20px", fontWeight: "300"

}

export default OrdersComponent;



