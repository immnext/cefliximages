import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import NProgress from "nprogress"


class OrderItemsComponent extends  Component{
    constructor(props){
        super(props)

        this.state = {
            data: [],
            httpstate:'0'
        }
    }


    componentDidMount(){

       this.getItems()
    }

    getItems = () => {

        let storagedata = JSON.parse(localStorage.getItem('authuser'));

        const headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get('/api/order/user/show-items',

            {
                headers: headers
            })

            .then( (response) => {
                if(response.data.data.length > 0) {


                    this.setState({data: response.data.data,httpstate:'2'})
                }else{
                    this.setState({data: response.data.data,httpstate:'1'})
                }

                NProgress.done();
            })
    }


    //render begins


    render() {

        if (this.state.httpstate == 0){
            return (
                <React.Fragment>
                    <div align="center" className="spinner_style">
                        <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                        ...loading....
                    </div>
                    <div className="empty-space col-xs-b35 col-md-b70"></div>
                </React.Fragment>
            )
        }

        if(this.state.httpstate == 2){
        return (


            <React.Fragment>

                <div className="swiper-wrapper">


                                    { this.state.data.map((item,index) => {

                                           return  <div key={index} className="swiper-slide">
                                                   <div className="icon-description-shortcode style-2">
                                                       <img className="image-icon image-thumbnail rounded-image" src={item.product_image} alt="" />
                                                       <div className="content">
                                                           <h6 className="title h6">{item.product_title}</h6>
                                                           <Link to={"/item/edit/"+item.product_id}>
                                                               <div className="button size-2 style-3 text-center">
                                                        <span className="button-wrapper">
                                                            <span className="icon"><img src="img/icon-4.png" alt=""/></span>
                                                            <span className="text">Edit Online</span>
                                                        </span>
                                                                   <input type="submit"/>
                                                               </div>
                                                           </Link>
                                                       </div>
                                                   </div>


                                               </div>


                                        })

                                    }

                    <div className="swiper-pagination relative-pagination"></div>


                </div>




            </React.Fragment>



        )}

         if(this.state.httpstate == 1) {
             return (
                 <React.Fragment>
                     <div className="alert alert-warning">
                         No Item found
                     </div>
                     <div className="empty-space col-xs-b35 col-md-b70"></div>
                 </React.Fragment>
             )
         }
    }
}

export default OrderItemsComponent;



