import React,{ Component } from 'react'
import { Link } from 'react-router-dom'
import StoreMenu from "../StoreMenu";
import StoreFooter from "../StoreFooter";

import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'


class StoreProfile extends Component{

    constructor(props){
        super(props)

        this.state = {
            data: [],
            http:0,
            name:"",
            storagedata :  JSON.parse(localStorage.getItem('authuser'))

        }
    }


    componentDidMount(){

        let storagedata = JSON.parse(localStorage.getItem('authuser'));


        const headers = {
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get("/api/store/show", {

                headers: headers
            }
        ).then((response) => {

            if(response.data.status) {
                this.setState({name: response.data.data.name,http:2})
            }else{
                this.setState({data: response.data.data,http:1})
            }

        })



    }

    handleChange = (event) => {

        this.setState({
            [event.target.name]:event.target.value
        })
    }


    handleSubmit  = (event) => {
        event.preventDefault();

        const MySwal = withReactContent(Swal);

        const headers = {
            'Authorization': 'Bearer '+this.state.storagedata.access_token,
        }

        NProgress.start();

        axios.post('/api/store/update', {
            name: this.state.name,
        },{
            headers: headers
        })
            .then( (response) => {

                if(response.data.status){
                    NProgress.done();

                    MySwal.fire({
                        type: 'success',
                        title: 'Success',
                        text: "Store successfully updated",

                    })



                }else{

                    NProgress.done();

                    this.setState({errormessages:[...this.state.errormessages,response.data.errors]})

                    // console.log(this.state.errormessages)
                }

            })
            .catch(function (error) {
                console.log(error);
            });



    }



    render(){



        if(this.state.http == 2)

            return(
                <div className="wrapper ">


                    <StoreMenu/>
                    <div className="main-panel">



                        <div className="content">
                            <div className="container-fluid">





                <div className="row">

                    <div className="col-md-8">
                        <div align="center">
                            {this.state.message}
                        </div>
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title">Edit Profile</h4>

                            </div>
                            <div className="card-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">

                                        <div className="col-md-12">
                                            <div className="form-group bmd-form-group">
                                                <label className="bmd-label-floating">Store Name</label>
                                                <input type="text" value={this.state.name} onChange={this.handleChange} name="name" className="form-control"/>
                                            </div>
                                        </div>

                                    </div>

                                    <button type="submit" className="btn btn-primary pull-right">Update Store Profile</button>
                                    <div className="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card card-profile">
                            <div className="card-avatar">
                                <Link to="user.html#pablo">
                                    <img className="img" src={this.state.data.profile_pic}/>
                                </Link>
                            </div>
                            <div className="card-body">
                                <h4 className="card-title"><i className="fa fa-user"></i> {this.state.name}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>



            )


        if(this.state.http == 0)
            return (
                <div className="wrapper ">


                    <StoreMenu/>
                    <div className="main-panel">



                        <div className="content">
                            <div className="container-fluid">





                                <div className="row">
                                <div className="col-md-12">

                <div align="center" style={{padding:"50px"}}>
                    <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            )


        if(this.state.http == 1)
            return (
                <div className="wrapper ">


                    <StoreMenu/>
                    <div className="main-panel">



                        <div className="content">
                            <div className="container-fluid">





                                <div className="row">

                <div className="card-body table-responsive">
                  No store
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
            )


    }

}


export default StoreProfile;
