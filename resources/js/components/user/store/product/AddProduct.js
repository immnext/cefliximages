import React,{ Component } from 'react'
import { Link } from 'react-router-dom'
import StoreMenu from "../StoreMenu";
import StoreFooter from "../StoreFooter";




class AddProduct extends Component{

    constructor(props){
        super(props)

        this.state = {
            title: "",
            description: "",
            price: "",
            progress:"",
            errormessages:[],
            storagedata :  JSON.parse(localStorage.getItem('authuser')),
            messagetype:"",
            messagedetails:""


        }
    }

    setValue = (event) => {

        this.setState({
            [event.target.name]:event.target.value
        })
    }


    handleSubmit  = (event) => {
        event.preventDefault();


        const headers = {
            'Authorization': 'Bearer '+this.state.storagedata.access_token,
        }

        axios.post('/api/estate/add', {
            name: this.state.name,
            address: this.state.address
        },{
            headers: headers
        })
            .then( (response) => {

                if(response.data.status){

                    this.setState({name:"",address:"",messagetype:"success",messagedetails:"Estate successfully created"})



                }else{

                    this.setState({errormessages:[...this.state.errormessages,response.data.errors]})

                    // console.log(this.state.errormessages)
                }

            })
            .catch(function (error) {
                console.log(error);
            });



    }


    handleAdd = (event) => {

        event.preventDefault()

        var output = document.getElementById('output');

        const headers = {
            'Authorization': 'Bearer '+this.state.storagedata.access_token,
        }

        let title = this.state.title
        let description = this.state.description
        let price = this.state.price

        document.getElementById('upload').onclick =  () => {

            var data = new FormData();

            data.append('title', this.state.title);
            data.append('description', this.state.description);
            data.append('price', this.state.price);
            data.append('image', document.getElementById('file').files[0]);
            data.append('product_file', document.getElementById('product_file').files[0]);

            var config = {
                onUploadProgress: (progressEvent) => {
                    var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
                    console.log(percentCompleted);
                    this.setState({progress:percentCompleted})
                },
                headers: headers
            };
            axios.post('/api/product/store', data, config,)
                .then(function (res) {
                    output.className = 'container';
                    output.innerHTML = res.data;
                })
                .catch(function (err) {
                    output.className = 'container text-danger';
                    output.innerHTML = err.message;
                });
        };



    }



    componentDidMount(){









    }

    render (){






        return (

            <React.Fragment>

                <div className="wrapper ">


                    <StoreMenu/>
                    <div className="main-panel">



                        <div className="content">
                            <div className="container-fluid">
                                <div className="row">


                                    <div className="col-md-6">
                                        <div align="center">
                                            {this.state.messagedetails}
                                        </div>

                                        <div align="center">
                                            {this.state.progress}
                                        </div>



                                        <div className="card">
                                            <div className="card-header card-header-primary">
                                                <div align="center">
                                                    {this.state.errormessage}
                                                </div>

                                                <div id="output" class="container"></div>

                                                <p className="card-category  text-center">Add Product</p>
                                            </div>

                                            <div className="card-body">


                                                <form onSubmit={this.handleAdd}>

                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <div className="form-group bmd-form-group">
                                                                <label className="">Name</label>
                                                                <input type="text" name="title"  className="form-control" value={this.state.title} onChange={this.setValue} required />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="form-group bmd-form-group">
                                                                <label className="">Description </label>
                                                                <input id="password" type="text" className="form-control" value={this.state.description} onChange={this.setValue} required  name="description"/>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="form-group bmd-form-group">
                                                                <label className="">Price </label>
                                                                <input id="password" type="text" className="form-control" value={this.state.price} onChange={this.setValue} required  name="price"/>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className=" ">
                                                                <label className="">Product Image </label>
                                                                <input type="file" id="file" className="form-control" onChange={this.onChange} />
                                                            </div>
                                                        </div>

                                                        <div className="col-md-12">
                                                            <div className="alert alert-info">Note that users would only be able to edit PSD files online, other files would be downloaded</div>
                                                            <div className=" ">
                                                                <label className="">Product File </label>
                                                                <input type="file" id="product_file" className="form-control" onChange={this.onChange} />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="progress">
                                                        <div className="progress-bar" role="progressbar" style={{"width": this.state.progress+"%"}} aria-valuenow={this.state.progress} aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>

                                                    <button type="submit" id="upload" className="btn btn-primary pull-right">Add</button>
                                                    <div className="clearfix"></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>



                                </div>


                            </div>
                        </div>

                       <StoreFooter/>

                    </div>

                    <div className="fixed-plugin"></div>






                            </div>







            </React.Fragment>

        );
    }
}



export default AddProduct;