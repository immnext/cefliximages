import React,{ Component } from 'react'
import { Link } from 'react-router-dom'
import StoreMenu from "../StoreMenu";
import StoreFooter from "../StoreFooter";




class AllSales extends Component{

    constructor(props){
        super(props)

        this.state = {
            data: [],
            storagedata :  JSON.parse(localStorage.getItem('authuser')),
            http:0

        }
    }




    componentDidMount(){


        let storagedata = JSON.parse(localStorage.getItem('authuser'));


        const headers = {
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get("/api/store/orders/success", {
                headers: headers
            }
        ).then((response) => {

            if(response.data.data.length > 0) {
                this.setState({data: response.data.data,http:2})
            }else{
                this.setState({data: response.data.data,http:1})
            }

        })






    }

    render (){


            return (

                <React.Fragment>

                    <div className="wrapper ">


                        <StoreMenu/>
                        <div className="main-panel">




                            <div className="content">
                                <div className="container-fluid">
                                    <div className="row">

                                        <div className="col-lg-12 col-md-12">
                                            <div className="card">
                                                <div className="card-header card-header-warning">
                                                    <h4 className="card-title">All Sales</h4>

                                                </div>

                                                <div className="card-body table-responsive">
                                                    <table className="table table-hover">
                                                        <thead className="text-warning">
                                                        <th>ID</th>
                                                        <th>Project Name</th>
                                                        <th>Price</th>
                                                        <th align="center">Image </th>
                                                        <th>Date Sold </th>

                                                        </thead>
                                                        <tbody>

                                                        { this.state.data.map((item,index) => {
                                                                return <tr key={index}>
                                                                    <td>{index +1}</td>
                                                                    <td>{item.product_title}</td>
                                                                    <td>{item.product_amount}</td>
                                                                    <td align="center"><img src={item.product_image} style={{width:"200px"}}/>
                                                                    </td>
                                                                    <td>{item.updated_at}</td>



                                                                </tr>
                                                            }

                                                        )
                                                        }

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>
                            </div>

                            <StoreFooter/>

                        </div>

                        <div className="fixed-plugin"></div>


                    </div>


                </React.Fragment>

            );
        }

}



export default AllSales;