import React,{Component} from "react"
import {Link} from 'react-router-dom'

//sweet alert begins
import Swal from 'sweetalert2'
import NProgress from "nprogress"
import withReactContent from 'sweetalert2-react-content'
//sweet alert ends


import Footer from "../../../includes/Footer";
import Header from "../../../includes/Header";
import StoreMenu from "../StoreMenu";
import StoreProducts from "./StoreProducts";
import RecentAddedProducts from "./RecentAddedProducts";
import RecentSoldProducts from "./RecentSoldProducts";
import TotalProducts from "./TotalProducts";
import TotalSuccessSales from "./TotalSuccessSales";


export default class UserStoreComponent extends Component {

    constructor(props){
        super(props)
    }


    addToCart(title,id,price,image){


        NProgress.start()
        const MySwal = withReactContent(Swal);

        let cartdata = JSON.parse(localStorage.getItem('cart'));

        if(cartdata != "" && cartdata !==null){


            var stored = JSON.parse(localStorage.getItem("cart"));

            var student2 = { id: id,itle:title,price:price,path:image };

            stored.push(student2);

            localStorage.setItem("cart", JSON.stringify(stored));

            var result = JSON.parse(localStorage.getItem("cart"));

            console.log("Existing cart updated")

        }else{

            var students = [];

            var student1 = { id: id,title:title,price:price,path:image };

            students.push(student1);

            localStorage.setItem("cart", JSON.stringify(students));
        }


        NProgress.done();

        MySwal.fire({

            type: 'success',
            title: 'Success',
            text: "Item Successfully added to cart",

        })

        // const datar = {
        //     id:response.data.status,
        // }
        //
        // localStorage.setItem('cart',JSON.stringify(datar));
    }






    
    render(){
        
        return (
            <div className="wrapper ">
               <StoreMenu/>
                <div className="main-panel">



                    <div className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-lg-4 col-md-6 col-sm-6">
                                    <div className="card card-stats">
                                        <div className="card-header card-header-warning card-header-icon">
                                            <div className="card-icon">
                                                <i className="material-icons">content_copy</i>
                                            </div>
                                            <p className="card-category">Balance </p>
                                            <h3 className="card-title">0.00

                                            </h3>
                                        </div>
                                        <div className="card-footer">

                                        </div>

                                    </div>
                                </div>

                               <TotalProducts/>

                               <TotalSuccessSales/>

                            </div>

                            <div className="row">

                                <div className="col-lg-12 col-md-12">
                                    <div className="card">
                                        <div className="card-header card-header-warning">
                                            <h4 className="card-title">Recently Added Products</h4>

                                        </div>

                                        <RecentAddedProducts/>
                                    </div>
                                </div>



                                <div className="col-lg-12 col-md-12">
                                    <div className="card">
                                        <div className="card-header card-header-warning">
                                            <h4 className="card-title">Recently sales</h4>

                                        </div>

                                        <RecentSoldProducts/>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                    <footer className="footer">
                        <div className="container-fluid">
                            <nav className="float-left">
                                <ul>
                                    <li>
                                        <Link to="https://www.creative-tim.com">
                                            Creative Tim
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="https://creative-tim.com/presentation">
                                            About Us
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="http://blog.creative-tim.com">
                                            Blog
                                        </Link>
                                    </li>
                                    <li>
                                        <Link to="https://www.creative-tim.com/license">
                                            Licenses
                                        </Link>
                                    </li>
                                </ul>
                            </nav>
                            <div className="copyright float-right">
                                &copy;
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>, made with <i className="material-icons">favorite</i> by
                                <Link to="https://www.creative-tim.com" target="_blank">Creative Tim</Link> for a better web.
                            </div>
                        </div>
                    </footer>
                </div>

        <div className="fixed-plugin"></div>
            </div>

        );
    }
}


