import React,{ Component } from 'react'
import { Link } from 'react-router-dom'


class RecentSoldProducts extends Component{

    constructor(props){
        super(props)

        this.state = {
            data: [],
            http:0

        }
    }


    componentDidMount(){

        let storagedata = JSON.parse(localStorage.getItem('authuser'));


        const headers = {
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get("/api/store/orders/success", {
            headers: headers
        }
        ).then((response) => {

            if(response.data.data.length > 0) {
                this.setState({data: response.data.data,http:2})
            }else{
                this.setState({data: response.data.data,http:1})
            }

        })



    }



    render(){



        if(this.state.http == 2)

        return(

            <div className="card-body table-responsive">
                <table className="table table-hover">
                    <thead className="text-warning">
                    <th>ID</th>
                    <th>Project Name</th>
                    <th>Price</th>
                    <th align="center">Image </th>
                    <th>Date Sold </th>

                    </thead>
                    <tbody>

                    { this.state.data.map((item,index) => {
                        return <tr key={index}>
                            <td>{index +1}</td>
                            <td>{item.product_title}</td>
                            <td>{item.product_amount}</td>
                            <td align="center"><img src={item.product_image} style={{width:"200px"}}/>
                            </td>
                            <td>{item.updated_at}</td>



                        </tr>
                            }

                        )
                    }

                    </tbody>
                </table>
            </div>

        )


        if(this.state.http == 0)
            return (
                <div align="center" style={{padding:"50px"}}>
                    <i className="fa fa-spinner fa-spin fa-3x"></i> <br/>
                </div>
            )


        if(this.state.http == 1)
            return (

                <div className="card-body table-responsive">
                    No sales yet in your store
                </div>
            )


    }

}


export default RecentSoldProducts;
