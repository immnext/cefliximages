import React,{ Component } from 'react'
import { Link } from 'react-router-dom'



class TotalSuccessSales extends Component{

    constructor(props){
        super(props)

        this.state = {
            data:[]

        }
    }


    componentDidMount(){

        let storagedata = JSON.parse(localStorage.getItem('authuser'));


        const headers = {
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get("/api/store/orders/success",{
            headers: headers
        }
        ).then((response) => {
            const datacount  = response.data.data.length;
           this.setState({data:datacount})

        })


    }

    render(){

        return (
        <div className="col-lg-4 col-md-6 col-sm-6">
            <div className="card card-stats">
                <div className="card-header card-header-danger card-header-icon">
                    <div className="card-icon">
                        <i className="fa fa-users"></i>
                    </div>
                    <p className="card-category">Total Sales</p>
                    <h3 className="card-title">{this.state.data}</h3>
                </div>
                <div className="card-footer"></div>
            </div>
        </div>


        )
    }

}


export default TotalSuccessSales;
