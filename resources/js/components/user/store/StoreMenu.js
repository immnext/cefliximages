import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';


class StoreMenu extends  Component{
    constructor(props){
        super(props)

    }




    componentDidMount(){

        let storagedata = JSON.parse(localStorage.getItem('authuser'));


        const headers = {
            'Authorization': 'Bearer '+storagedata.access_token,
        }


        axios.get("/api/store/show", {

                headers: headers
            }
        ).then((response) => {

            if(response.data.status) {
                this.setState({name: response.data.data.name,http:2})
            }else{
                this.setState({data: response.data.data,http:1})
            }

        })



    }







    render() {



        return (

            <React.Fragment>
                <div className="sidebar" data-color="purple" data-background-color="white" data-image="/assets/img/sidebar-1.jpg">

                    <div className="logo">
                        <Link to="#" className="simple-text logo-normal">
                           My Store
                        </Link>
                    </div>
                    <div className="sidebar-wrapper">
                        <ul className="nav">
                            <li className="nav-item active  ">
                                <Link className="nav-link" to="/owner/store/dashboard">
                                    <i className="material-icons">dashboard</i>
                                    <p>Dashboard</p>
                                </Link>
                            </li>
                            <li className="nav-item ">
                                <Link className="nav-link" to="/owner/store/profile">
                                    <i className="material-icons">person</i>
                                    <p>Store Profile</p>
                                </Link>
                            </li>

                            <li className="nav-item ">
                                <Link className="nav-link" to="/owner/store/addproduct">
                                    <i className="material-icons">content_paste</i>
                                    <p>Add Product</p>
                                </Link>
                            </li>

                            <li className="nav-item ">
                                <Link className="nav-link" to="/owner/store/products">
                                    <i className="material-icons">content_paste</i>
                                    <p>All Products </p>
                                </Link>
                            </li>

                            <li className="nav-item ">
                                <Link className="nav-link" to="/owner/store/sales">
                                    <i className="material-icons">library_books</i>
                                    <p>All Sales</p>
                                </Link>
                            </li>


                            <li className="nav-item  ">
                                <Link className="nav-link" to="/user/profile" target="_blank">
                                    <i className="material-icons">unarchive</i>
                                    <p>User Profile </p>
                                </Link>
                            </li>

                            <li className="nav-item active-pro ">
                                <Link className="nav-link" to="/" target="_blank">
                                    <i className="material-icons">unarchive</i>
                                    <p>Back to Site </p>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </React.Fragment>
        )

    }
}

export default StoreMenu;



