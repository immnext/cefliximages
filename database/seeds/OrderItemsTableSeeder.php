<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders  = \App\Model\Order::all();
        $order_ids = array();


        foreach($orders as $order){
            $order_ids[] =  $order->unique_id;
        }



        $stores  = \App\Model\Store::all();
        $store_ids = array();


        foreach($stores as $store){
            $store_ids[] =  $store->unique_id;
        }




        $products  = \App\Model\Product::all();
        $products_ids = array();
        $product_amounts = array();

        foreach($products as $product){
            $products_ids[] =  $product->unique_id;
            $product_amounts[] = $product->price;
        }






        $faker = Faker::create('\App\Model\OrderItem');

        for($i = 0;$i < 100;$i++) {
            $word = $faker->word;
            \App\Model\OrderItem::insert([
                'order_id' => $faker->randomElement($order_ids),
                'store_id' => $faker->randomElement($store_ids),
                'product_id' => $faker->randomElement($products_ids),
                'product_amount' => $faker->randomElement($product_amounts),
                'created_at' => $faker->dateTime()

            ]);

        }
    }
}
